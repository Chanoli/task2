package base;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.junit.jupiter.api.Test;

import base.PictureFrame.DominoPanel;

class PictureFrameTest {

	@Test
	void test() {
		class PictureFrame {

		    public int[] rerollArray = null;
		    public Main master = null;
		    public DominoPanel dp;

		    class DominoPanel extends JPanel {
		        private static final long serialVersionUID = 4190229282411119364L;

		        void drawGrid(Graphics g) {
		            // Existing code for drawing the grid
		        }

		        void drawHeadings(Graphics g) {
		            // Existing code for drawing headings
		        }

		        void drawDomino(Graphics g, Domino d) {
		            // Existing code for drawing domino
		        }

		        // Existing code for drawing digits and filling digits
		    }

		    public PictureFrame(Main sf) {
		        master = sf;
		        createPictureFrame();
		    }

		    private void createPictureFrame() {
		        if (dp == null) {
		            JFrame frame = new JFrame("Abominodo");
		            dp = new DominoPanel();
		            frame.setContentPane(dp);
		            frame.pack();
		            frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		            frame.setVisible(true);
		        }
		    }

		    public void reset() {
		        // TODO Auto-generated method stub
		    }
		}
	}

}
