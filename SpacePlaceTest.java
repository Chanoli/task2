package base;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SpacePlaceTest {

	@Test
	void test() {

		class SpacePlace {
		    private int xOrg;
		    private int yOrg;
		    private double theta;
		    private double phi;

		    public SpacePlace() {
		        xOrg = 0;
		        yOrg = 0;
		    }

		    public SpacePlace(double theta, double phi) {
		        this.theta = theta;
		        this.phi = phi;
		    }

		    public int getXOrg() {
		        return xOrg;
		    }

		    public void setXOrg(int xOrg) {
		        this.xOrg = xOrg;
		    }

		    public int getYOrg() {
		        return yOrg;
		    }

		    public void setYOrg(int yOrg) {
		        this.yOrg = yOrg;
		    }

		    public double getTheta() {
		        return theta;
		    }

		    public void setTheta(double theta) {
		        this.theta = theta;
		    }

		    public double getPhi() {
		        return phi;
		    }

		    public void setPhi(double phi) {
		        this.phi = phi;
		    }
		}

	}

}
