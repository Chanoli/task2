package base;

import static org.junit.jupiter.api.Assertions.*;

import java.net.InetAddress;

import org.junit.jupiter.api.Test;

class ConnectionGeniusTest {

	@Test
	void test() {
		class ConnectionGenius {

		    private InetAddress hostAddress;

		    public ConnectionGenius(InetAddress hostAddress) {
		        this.hostAddress = hostAddress;
		    }

		    public void startGame() {
		        downloadWebVersion();
		        connectToWebService();
		        readyToPlay();
		    }

		    private void downloadWebVersion() {
		        System.out.println("Getting specialized web version.");
		        System.out.println("Please wait a moment.");
		    }

		    private void connectToWebService() {
		        System.out.println("Connecting to web service.");
		    }

		    private void readyToPlay() {
		        System.out.println("Ready to play!");
		    }
		}
	}

}
