package base;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import base.MultiLingualStringTable.Language;

class MultiLingualStringTableTest {

	@Test
	void test() {
		class MultiLingualStringTable {

		    private static Language currentLanguage = Language.ENGLISH;

		    private static String[] englishMessages = {"Enter your name:", "Welcome", "Have a good time playing Abominodo"};
		    private static String[] klingonMessages = {"'el lIj pong:", "nuqneH", "QaQ poH Abominodo"};

		    public static String getMessage(int index) {
		        switch (currentLanguage) {
		            case ENGLISH:
		                return englishMessages[index];
		            case KLINGON:
		                return klingonMessages[index];
		            default:
		                throw new IllegalArgumentException("Unsupported language setting");
		        }
		    }
		}
	}

}
