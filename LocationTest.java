package base;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;

import base.Location.DIRECTION;

class LocationTest {

	@Test
	void test() {
		class Location extends SpacePlace {

		    public static int c;
		    public int r;
		    public DIRECTION d;
		    public int tmp;
		    

		    public Location(int r, int c) {
		        this.r = r;
		        this.c = c;
		    }

		    public Location(int r, int c, DIRECTION d) {
		        this(r, c);
		        this.d = d;
		    }

		    public String toString() {
		        tmp = c + 1;
		        if (d == null) {
		            return "(" + tmp + "," + (r + 1) + ")";
		        } else {
		            return "(" + tmp + "," + (r + 1) + "," + d + ")";
		        }
		    }

		    public void drawGridLines(Graphics g) {
		        g.setColor(Color.LIGHT_GRAY);
		        final int GRID_SIZE = 7;
		        final int CELL_SIZE = 20;

		        for (tmp = 0; tmp <= GRID_SIZE; tmp++) {
		            g.drawLine(20, 20 + tmp * CELL_SIZE, 180, 20 + tmp * CELL_SIZE);
		            g.drawLine(20 + tmp * CELL_SIZE, 20, 20 + tmp * CELL_SIZE, 160);
		        }
		    }

		    public static int getInt() {
		        try (BufferedReader r = new BufferedReader(new InputStreamReader(System.in))) {
		            do {
		                try {
		                    return Integer.parseInt(r.readLine());
		                } catch (IOException e) {
		                    // Handle the exception
		                    e.printStackTrace();
		                }
		            } while (true);
		        } catch (IOException e) {
		            // Handle the exception
		            e.printStackTrace();
		        }
				return c;
		    }
		}
	}

}
